<?php

namespace Yangpeng\ThinkCaptcha\facade;

use think\Facade;

class Captcha extends Facade
{
    protected static function getFacadeClass()
    {
        return \Yangpeng\ThinkCaptcha\Captcha::class;
    }
}