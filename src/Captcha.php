<?php

namespace Yangpeng\ThinkCaptcha;

use Exception;
use think\Config;
use think\Session;

class Captcha extends \think\captcha\Captcha
{

    /**
     * @var Session|null
     */
    private $session;


    /**
     * 架构方法 设置参数
     * @access public
     * @param Config $config
     * @param Session $session
     */
    public function __construct(Config $config, Session $session)
    {
        parent::__construct($config, $session);
        $this->session = $session;
    }

    /**
     * 创建验证码
     * @return array
     * @throws Exception
     */
    protected function generate(): array
    {
        $bag = '';

        if ($this->math) {
            $this->useZh = false;
            $this->length = 5;

            $x = random_int(10, 30);
            $y = random_int(1, 9);
            $operator_arr = ['+', '-'];
            $operator = $operator_arr[array_rand($operator_arr, 1)];
            switch ($operator) {
                case '+':
                    $bag = "{$x} + {$y} = ";
                    $key = $x + $y;
                    break;
                case '-':
                    $bag = "{$x} - {$y} = ";
                    $key = $x - $y;
                    break;
            }
            $key .= '';
        } else {
            if ($this->useZh) {
                $characters = preg_split('/(?<!^)(?!$)/u', $this->zhSet);
            } else {
                $characters = str_split($this->codeSet);
            }

            for ($i = 0; $i < $this->length; $i++) {
                $bag .= $characters[random_int(0, count($characters) - 1)];
            }

            $key = mb_strtolower($bag, 'UTF-8');
        }

        $hash = password_hash($key, PASSWORD_BCRYPT, ['cost' => 10]);

        $this->session->set('captcha', [
            'key' => $hash,
        ]);

        return [
            'value' => $bag,
            'key' => $hash,
        ];
    }
}